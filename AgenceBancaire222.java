package fr.umfds.oralCourriel;

public class Courriel {
String destination;
String titre;
String corps;
String pieceJointe;

public Courriel() {}

public Courriel(String destination, String titre, String corps, String pieceJointe) {
	this.destination = destination;
	this.titre = titre;
	this.corps = corps;
	this.pieceJointe = pieceJointe;
}

//getters
public String getDestination() {
	return destination;
}

public String getTitre() {
	return titre;
}

public String getCorps() {
	return corps;
}

public String getPieceJointe() {
	return pieceJointe;
}

//setters
public void setDestination(String destination) {
	this.destination = destination;
}


public void setTitre(String titre) {
	this.titre = titre;
}

public void setCorps(String corps) {
	this.corps = corps;
}

public void setPieceJointe(String pieceJointe) {
	this.pieceJointe = pieceJointe;
}

//Méthodes
private boolean MailPresent() {
    return !(destination.length()==0);
}

private boolean MailBienForme() {
    return destination.matches("[a-zA-Z][a-zA-Z0-9]*@[a-zA-Z]+\\.+[a-zA-Z]+");
}

private boolean PresenceTitre() {
    return !(titre.length()==0);
}

private boolean ContainsAbrevPJ() {
    return corps.matches("(.*)(joint|jointe|PJ)(.*)");
}

private boolean PjPresente() {
    return !(pieceJointe.length() == 0);
}
public void envoyer() throws EnvoiEchoue{
	//Verification de la présence de l'email
		if(!MailPresent()) {
			throw new EnvoiEchoue("destinataire vide");
		}
		//Verification que l'email est bien formé
		if(!MailBienForme()) {
            throw new EnvoiEchoue("Mail incorrect");
        }
	//Verification de la présence du titre:
		if(!PresenceTitre()) {
			throw new EnvoiEchoue("Absence du titre");
			}
	//Verification si corps contient abrev, la présence de la PJ
		if (ContainsAbrevPJ() && !PjPresente()) {
			throw new EnvoiEchoue ("Pas de Pièce jointe");
		}
}
}
